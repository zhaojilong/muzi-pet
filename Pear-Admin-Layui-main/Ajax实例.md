            $.ajax({
                url: 'http://localhost:5161/api/ClassFy/DelClassFy/'+obj.data.id,
                dataType:'json',
                type:'delete',
                headers:{
                    "Authorization":sessionStorage.getItem("token")
                },
                success:function(result){
                    layer.close(loading);
                    if(result.result){
                        layer.msg(result.msg,{icon:1,time:1000},function(){
                            obj.del();
                        });
                    }else{
                        layer.msg(result.msg,{icon:2,time:1000});
                    }
                }
            })
            
            layer.confirm('确定要删除该分类', {icon: 3, title:'提示'}, function(index){
            layer.close(index);
            let loading = layer.load();
            $.ajax({
                url: 'http://localhost:5161/api/ClassFy/DelClassFy/'+obj.data.id,
                dataType:'json',
                type:'delete',
                headers:{
                    "Authorization":sessionStorage.getItem("token")
                },
                success:function(result){
                    layer.close(loading);
                    if(result.result){
                        layer.msg(result.msg,{icon:1,time:1000},function(){
                            obj.del();
                        });
                    }else{
                        layer.msg(result.msg,{icon:2,time:1000});
                    }
                }
            })
        });