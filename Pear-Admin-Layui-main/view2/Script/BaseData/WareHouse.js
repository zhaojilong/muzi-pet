
layui.use(['table', 'form', 'upload', 'layer', 'laydate', 'jquery', 'util', 'drawer', 'dropdown','popup','treetable'], function () {
    var table = layui.table,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$,
        layer = layui.layer,
        upload = layui.upload
        , util = layui.util;
        let drawer = layui.drawer;
		let dropdown = layui.dropdown;
        var popup = layui.popup;
        let treetable = layui.treetable;

        form.on('submit(user-query)', function(data) {
            table.reload('user-table', {
                where: data.field
            })
           
            return false;
        });

        form.on('submit(user-add)', function(data) {
            layer.open({
                title: '添加',
                type: 2,
                area: ['80%','80%'],
                content: '../../../view2/BaseData/WareHouseadd.html'//'<form class="layui-form" action="" onsubmit="return false" style="margin-top: 19px;"><div class="layui-form-item"><label class="layui-form-label">仓库编号</label><div class="layui-input-inline"> <input type="text" name="vbare" required  lay-verify="required"placeholder="请输入输入框内容" autocomplete="off" class="layui-input"> </div> </div><div class="layui-form-item"><label class="layui-form-label">仓库名称</label><div class="layui-input-inline"><input type="text" name="zt93k" required  lay-verify="required"placeholder="请输入输入框内容" autocomplete="off" class="layui-input"></div></div><div class="layui-form-item"><div class="layui-input-block"><button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button><button type="reset" class="layui-btn layui-btn-primary">重置</button></div></div></form>'
              });
            table.reload('user-table', {
                where: data.field
            })
           
            return false;
        });
        table.render({
            elem: '#user-table',
            url: 'http://localhost:5161/api/WareHouse/GetAll',
            //page: true,
            method:'post', //请求方式
            contentType:'application/json',
            where :{
                "name":""
            },
            headers:{
                "Authorization":sessionStorage.getItem("token")
            },
            cols: [[               
                {title: '序号',field: 'id',align: 'center',width: 100},
                {title: '编号', field: 'idCode', align: 'center', width: 80, edit: 'text'},
                {title: '仓库',field: 'name',align: 'center',width: 500, edit: 'text'} , 
                {fixed: 'right', title:'操作', width: 125, minWidth: 125, toolbar: '#barDemo'}
            ]],
            skin: 'line',          
            
            done: function(res, curr, count) {
               
            }
            
        });

        // 单元格编辑后的事件
        table.on('edit(user-table)', function(obj){
            var value = obj.value //得到修改后的值
            ,data = obj.data //得到所在行所有键值
            ,field = obj.field; //得到字段
            //layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改值为：'+ util.escape(value));
            $.ajax({
                    type: "put",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'http://localhost:5161/api/WareHouse/Put',
                    data:JSON.stringify(data),
                    headers:{
                        "Authorization":sessionStorage.getItem("token")
                    },
                    success: function (response) {
                        if(response.result){
                            popup.success("修改成功");
                        }else{
                            popup.failure("修改失败:"+data.msg);
                        }
                    }
                });
        });
        //删除
         //触发单元格工具事件
        table.on('tool(user-table)', function(obj){ // 双击 toolDouble
            var data = obj.data;
            //console.log(obj)
            if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                $.ajax({
                    type: "delete",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: 'http://localhost:5161/api/WareHouse/Delete/'+ data.id,
                    //data:JSON.stringify(data),
                    headers:{
                        "Authorization":sessionStorage.getItem("token")
                    },
                    success: function (response) {
                        if(response.result){
                            popup.success("删除成功");
                        }else{
                            popup.failure("删除失败:"+data.msg);
                        }
                    }
                });
                obj.del();
                layer.close(index);
            });
            
            }
        });
});