
layui.use(['table', 'form', 'upload', 'layer', 'laydate', 'jquery', 'util', 'drawer', 'dropdown','popup','treetable', 'dtree'], function () {
    var table = layui.table,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$,
        layer = layui.layer,
        upload = layui.upload
        , util = layui.util;
        let drawer = layui.drawer;
		let dropdown = layui.dropdown;
        var popup = layui.popup;
        let treetable = layui.treetable;
        let dtree = layui.dtree;
        let MODULE_PATH = "operate/";
   
        
        //查询表格        
        table.render({
            elem: '#organization-table',
            url: 'http://localhost:5161/api/BusinessUnit/GetUnit',
            headers:{"Authorization":sessionStorage.getItem("token")},
            where:{
                "name": $("#realName").val(),
                "category": $("#Unit").val(),//种类
                "status": $("#Start").val()//状态
              },
            method:'post', //请求方式
            contentType:'application/json',
            height: 'full-150',
            page: true,
            cols: [[
                {type: 'checkbox'},
                {title: '名称',field: 'unitName',width : 110,align: 'center' },
                {title: '简拼',field: 'unitNameJp',width : 140,align: 'center'},
                {title: '类型',field: 'category',width : 110,align: 'center',templet:function(d){if(d.category==0){return '供应商/客户'}else if(d.category==1){return '供应商'}else{return '客户'}}},
                {title: '电话',field: 'companyPhone',width : 110,align: 'center'},
                {title: '地址',field: 'address',width : 110,align: 'center'},
                {title: '传真',field: 'fax',width : 110,align: 'center'},
                {title: '卡号',field: 'cardNo',width : 110,align: 'center'},
                {title: '状态',field: 'status',width : 110,align: 'center',templet:function(d){if(d.status==0){return '启用'}else{return '禁用'}}},
                {title: '备注',field: 'unitDescription',width : 110,align: 'center'},
                
                {fixed: 'right', title:'操作', width: 125, minWidth: 125, toolbar: '#organization-bar'}
            ]],
            skin: 'line',
            toolbar: '#organization-toolbar',
            defaultToolbar: [{
                title: '刷新',
                layEvent: 'refresh',
                icon: 'layui-icon-refresh',
            }, 'filter', 'print', 'exports']
        });
        //查询
        $("#query").click(function(e) {
            table.reload("organization-table",{
                where:{
                    "name": $("#realName").val(),
                "category": $("#Unit").val(),//种类
                "status": $("#Start").val()//状态
                  }
            });
        });

        table.on('tool(organization-table)', function(obj) {
            if (obj.event === 'remove') {
                window.remove(obj);
            } else if (obj.event === 'edit') {
                window.edit(obj);
            }
        });

        table.on('toolbar(organization-table)', function(obj) {
            if (obj.event === 'add') {
                window.add();
            } else if (obj.event === 'sh') {
                window.sh();
            } 
        });

        window.add = function() {
            layer.open({
                type: 2,
                title: '新增',
                shade: 0.1,
                area: ['70%', '80%'],
                content: MODULE_PATH + '../BUsinessUnitAdd.html'
            });
        }

        window.edit = function(obj) {
            //layer.msg(obj.data.id);
            layer.open({
                type: 2,
                title: '修改',
                shade: 0.1,
                area: ['70%', '80%'],
                content: MODULE_PATH+'../BUsinessUnitEdit.html?id='+obj.data.id
            });
        }

        window.remove = function(obj) {
            layer.confirm('确定要删除该用户', {
                icon: 3,
                title: '提示'
            }, function(index) {
                layer.close(index);
                let loading = layer.load();
                $.ajax({
                    url: 'http://localhost:5161/api/BusinessUnit/DelUnit?id='+obj.data.id,
                    dataType: 'json',
                    headers:{"Authorization":sessionStorage.getItem("token")},
                    type: 'delete',
                    success: function(result) {
                        layer.close(loading);
                        if (result.isSuccess) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function() {
                                obj.del();
                            });
                        } else {
                            layer.msg(result.msg, {
                                icon: 2,
                                time: 1000
                            });
                        }
                    }
                })
            });
        }
    window.sh=function(obj){
        //获取选中数据
        var checkStatus = table.checkStatus('organization-table')
        ,data = checkStatus.data 
        ,arr_id = new Array();
        for(var i = 0;i<data.length;i++){
        arr_id.push(data[i].id);
        }
        if(arr_id.length>1){
            layer.msg("不允许多条审核");
        }else if(arr_id==""){
            layer.msg("请选中审核的数据");
        }
        else{
            //alert("进入");
            $.ajax({
                type: "put",
                url: "http://localhost:5161/api/BusinessUnit/PutStart?id="+arr_id,
                headers:{"Authorization":sessionStorage.getItem("token")},
                dataType: "json",
                success: function (result) {
                    if (result.isSuccess) {
                        layer.msg(result.msg, {
                            icon: 1,
                            time: 1000
                        }, function() {
                            location.reload();
                        });
                    } else {
                        layer.msg(result.msg, {
                            icon: 2,
                            time: 1000
                        });
                    }
                }
            });
        }
        
    }
       
    //双击查看
    table.on('rowDouble(organization-table)', function(obj){
        //obj 同上
        var data=obj.data;
        //标注选中样式
        //obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click') ;
        window.edit(obj);
      });
});
