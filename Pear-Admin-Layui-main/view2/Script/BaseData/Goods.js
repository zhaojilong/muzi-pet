
layui.use(['table', 'form', 'upload', 'layer', 'laydate', 'jquery', 'util', 'drawer', 'dropdown','popup','treetable', 'dtree'], function () {
    var table = layui.table,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$,
        layer = layui.layer,
        upload = layui.upload
        , util = layui.util;
        let drawer = layui.drawer;
		let dropdown = layui.dropdown;
        var popup = layui.popup;
        let treetable = layui.treetable;
        let dtree = layui.dtree;
        let MODULE_PATH = "operate/";
    //左侧菜单
        $.ajax({
            url: 'http://localhost:5161/api/ClassFy/GetClassFy_Fj',
            headers:{"Authorization":sessionStorage.getItem("token")},
            type: 'get',
            dataType: 'json',
            success: function(res) {
                DTree(res.data);
            }
          });
          var DTree = function(data) {  
            dtree.render({
            elem: "#organizationTree",
            data: data,
            initLevel: "2", //默认展开层级为1
            line: true, // 有线树
            ficon: ["1", "-1"], // 设定一级图标样式。0表示方形加减图标，8表示小圆点图标
            icon: ["0", "2"], // 设定二级图标样式。0表示文件夹图标，5表示叶子图标
            method: 'get',
            //url: "../../admin/data/organizationtree.json"
        }) };
        
        //查询表格        
        table.render({
            elem: '#organization-table',
            url: 'http://localhost:5161/api/Goods/GetListGoods',
            headers:{"Authorization":sessionStorage.getItem("token")},
            where:{
                "goods_Tj":"",                
                "classFy_id": -1
            },
            method:'post', //请求方式
            contentType:'application/json',
            height: 'full-150',
            page: true,
            cols: [[
                {type: 'checkbox'},
                {title: '分类名称',field: 'classFy_Name',width : 110,align: 'center' },
                {title: '商品名称',field: 'goods_Name',width : 140,align: 'center'},
                {title: '商品简码',field: 'goods_JpName',width : 110,align: 'center'},
                {title: '商品编号',field: 'goods_Bh',width : 110,align: 'center'},
                {title: '商品规格',field: 'goods_Gg',width : 110,align: 'center'},
                {title: '商品单位',field: 'goods_Unit',width : 110,align: 'center'},
                {title: '销售价1',field: 'goods_Price1',width : 110,align: 'center'},
                {title: '销售价2',field: 'goods_Price2',width : 110,align: 'center'},
                {title: '销售价3',field: 'goods_Price3',width : 110,align: 'center'},
                {title: '图片',field: 'imgPath',align: 'center',width : 80,templet:'#imgTpl'},
                {title: '描述',field: 'description',width : 110,align: 'center'},
                {title: '创建日期',field: 'createDate',width : 170,align: 'center'},
                     
                {title: '内部编号',field: 'id',width : 100,align: 'center'},
                {fixed: 'right', title:'操作', width: 125, minWidth: 125, toolbar: '#organization-bar'}
            ]],
            skin: 'line',
            toolbar: '#organization-toolbar',
            defaultToolbar: [{
                title: '刷新',
                layEvent: 'refresh',
                icon: 'layui-icon-refresh',
            }, 'filter', 'print', 'exports']
        });
        // 绑定节点点击事件
				dtree.on("node(organizationTree)", function(obj) {
					// if (!obj.param.leaf) { 放开不能点击父目录
					// 	var $div = obj.dom;
					// 	DTree.clickSpread($div); //调用内置函数展开节点
					// } else {
						//layer.msg("叶子节点就不展开了,刷新右侧列表   "+obj.param.nodeId);
						table.reload("organization-table",{
                            where:{
                                "goods_Tj":$("#realName").val(),                
                                "classFy_id": obj.param.nodeId
                            }
                        });
                        $("#id").val(obj.param.nodeId);
					//}
				});
        //查询
        $("#query").click(function(e) {
            table.reload("organization-table",{
                where:{
                    "goods_Tj":$("#realName").val(),                
                    "classFy_id": $("#id").val()
                }
            });
        });

        table.on('tool(organization-table)', function(obj) {
            if (obj.event === 'remove') {
                window.remove(obj);
            } else if (obj.event === 'edit') {
                window.edit(obj);
            }
        });

        table.on('toolbar(organization-table)', function(obj) {
            if (obj.event === 'add') {
                window.add();
            } else if (obj.event === 'refresh') {
                window.refresh();
            } else if (obj.event === 'batchRemove') {
                window.batchRemove(obj);
            }
        });
        window.add = function() {
            layer.open({
                type: 2,
                title: '新增',
                shade: 0.1,
                area: ['70%', '80%'],
                content: MODULE_PATH + '../GoodsAdd.html?flid='+$("#id").val()
            });
        }

        window.edit = function(obj) {
            //layer.msg(obj.data.id);
            layer.open({
                type: 2,
                title: '修改',
                shade: 0.1,
                area: ['70%', '80%'],
                content: MODULE_PATH+'../GoodsEdit.html?id='+obj.data.id
            });
        }

        window.remove = function(obj) {
            layer.confirm('确定要删除该用户', {
                icon: 3,
                title: '提示'
            }, function(index) {
                layer.close(index);
                let loading = layer.load();
                $.ajax({
                    url: 'http://localhost:5161/api/Goods/GoodsDel?id='+obj.data.id,
                    dataType: 'json',
                    headers:{"Authorization":sessionStorage.getItem("token")},
                    type: 'post',
                    success: function(result) {
                        layer.close(loading);
                        if (result.isSuccess) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function() {
                                obj.del();
                            });
                        } else {
                            layer.msg(result.msg, {
                                icon: 2,
                                time: 1000
                            });
                        }
                    }
                })
            });
        }

        window.batchRemove = function(obj) {
            let data = table.checkStatus(obj.config.id).data;
            if (data.length === 0) {
                layer.msg("未选中数据", {
                    icon: 3,
                    time: 1000
                });
                return false;
            }
            let ids = "";
            for (let i = 0; i < data.length; i++) {
                ids += data[i].organizationId + ",";
            }
            ids = ids.substr(0, ids.length - 1);
            layer.confirm('确定要删除这些用户', {
                icon: 3,
                title: '提示'
            }, function(index) {
                layer.close(index);
                let loading = layer.load();
                $.ajax({
                    url: MODULE_PATH + "batchRemove/" + ids,
                    dataType: 'json',
                    type: 'delete',
                    success: function(result) {
                        layer.close(loading);
                        if (result.success) {
                            layer.msg(result.msg, {
                                icon: 1,
                                time: 1000
                            }, function() {
                                table.reload('organization-table');
                            });
                        } else {
                            layer.msg(result.msg, {
                                icon: 2,
                                time: 1000
                            });
                        }
                    }
                })
            });
        }

        window.refresh = function(param) {
            table.reload('organization-table');
        }
        table.on('rowDouble(organization-table)', function(obj){
            //obj 同上
            var data=obj.data;
            //标注选中样式
            //obj.tr.addClass('layui-table-click').siblings().removeClass('layui-table-click') ;
            window.edit(obj);
          });
});
