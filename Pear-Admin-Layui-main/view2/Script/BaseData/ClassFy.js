
layui.use(['table', 'form', 'dtree', 'upload', 'layer', 'laydate', 'jquery', 'util', 'drawer', 'dropdown','popup','treetable'], function () {
    var table = layui.table,
        laydate = layui.laydate,
        form = layui.form,
        $ = layui.$,
        layer = layui.layer,
        upload = layui.upload
        , util = layui.util;
        let drawer = layui.drawer;
		let dropdown = layui.dropdown;
        var popup = layui.popup;
        let treetable = layui.treetable;
        

        $.ajax({
            url: 'http://localhost:5161/api/ClassFy/GetClassFy',
            headers:{"Authorization":sessionStorage.getItem("token")},
            type: 'get',
            dataType: 'json',
            success: function(res) {
              renderTable(res.data);
            }
          })
        var renderTable=function(data){
            treetable.render({
            treeColIndex: 1,
            treeSpid: -1,
            treeIdName: 'id',
            treePidName: 'pid',
            skin:'line',
            treeDefaultClose: true,
            toolbar:'#power-toolbar',
            elem: '#power-table',
            data:data,
            page: false,
            cols: [
                [
                {type: 'checkbox'},
                {field: 'name', minWidth: 200, title: '分类名称'},                
                {field: 'num', title: '排序'},
                {title: '操作',templet: '#power-bar', width: 150, align: 'center'}
                ]
            ]
    });}

    table.on('tool(power-table)',function(obj){
        if (obj.event === 'remove') {
            window.remove(obj);
        } else if (obj.event === 'add') {
            window.add(obj);
        }else if (obj.event === 'edit') {
            window.edit(obj);
        }
    })

    table.on('toolbar(power-table)', function(obj){
        //alert(obj.event);
        if(obj.event === 'add'){
            window.add();
        } else if(obj.event === 'refresh'){
            window.refresh();
        } else if(obj.event === 'batchRemove'){
            window.batchRemove(obj);
        } else if(obj.event === 'expandAll'){
             treetable.expandAll("#power-table");
        } else if(obj.event === 'foldAll'){
             treetable.foldAll("#power-table");
        } else if(obj.event === 'reload'){
             treetable.reload("#power-table");
        }else if(obj.event === 'edit'){
            window.edit();
       }
    });
    
    form.on('submit(power-query)', function(data) {
        var keyword = data.field.keyword;
        treetable.search('#power-table',keyword);
        return false;
    });

    window.add = function(obj){
        layer.open({
            type: 2,
            title: '新增',
            shade: 0.1,
            area: ['450px', '500px'],
            content: '../../../view2/BaseData/ClassFyAdd.html?name='+ obj.data.name +'&id='+ obj.data.id
        });
    }
    window.edit = function(obj){
        layer.open({
            type: 2,
            title: '修改',
            shade: 0.1,
            area: ['450px', '500px'],
            content: '../../../view2/BaseData/ClassFyEdit.html?name='+ obj.data.name +'&id='+ obj.data.id+'&num='+ obj.data.num
        });
    }
    // 单元格编辑后的事件
    table.on('edit(power-table)', function(obj){
        var value = obj.value //得到修改后的值
        ,data = obj.data //得到所在行所有键值
        ,field = obj.field; //得到字段
        //layer.msg('[ID: '+ data.id +'] ' + field + ' 字段更改值为：'+ util.escape(value));
        $.ajax({
                type: "put",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                url: 'http://localhost:5161/api/ClassFy/EditClassFy',
                data:JSON.stringify(data),
                headers:{
                    "Authorization":sessionStorage.getItem("token")
                },
                success: function (response) {
                    if(response.result){
                        popup.success("修改成功");
                    }else{
                        popup.failure("修改失败:"+data.msg);
                    }
                }
            });
    });
    window.remove = function(obj){
        layer.confirm('确定要删除该分类', {icon: 3, title:'提示'}, function(index){
            layer.close(index);
            let loading = layer.load();
            $.ajax({
                url: 'http://localhost:5161/api/ClassFy/DelClassFy/'+obj.data.id,
                dataType:'json',
                type:'delete',
                headers:{
                    "Authorization":sessionStorage.getItem("token")
                },
                success:function(result){
                    layer.close(loading);
                    if(result.result){
                        layer.msg(result.msg,{icon:1,time:1000},function(){
                            obj.del();
                        });
                    }else{
                        layer.msg(result.msg,{icon:2,time:1000});
                    }
                }
            })
        });
    }
    
});