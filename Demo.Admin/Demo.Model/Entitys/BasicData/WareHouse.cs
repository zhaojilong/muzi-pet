﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Entitys.BasicData
{
    /// <summary>
    /// 仓库
    /// </summary>
    public class WareHouse : IBase
    {
        /// <summary>
        /// 仓库名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 仓库编号
        /// </summary>
        public string IDCode { get; set; }
    }
}
