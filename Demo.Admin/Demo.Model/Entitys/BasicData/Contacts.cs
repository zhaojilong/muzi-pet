﻿using Demo.Model.Common;
using OracleInternal.SqlAndPlsqlParser.LocalParsing;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Entitys.BasicData
{
    public class Contacts : IBase
    {
        [SugarColumn(ColumnDescription = "单位id")]
        public long UnitId { get; set; }
        [SugarColumn(ColumnDescription = "名称")]
        public string Name { get; set; }
        [SugarColumn(ColumnDescription = "邮箱")]
        public string Email { get; set; }
        [SugarColumn(ColumnDescription = "电话")]
        public string Phone { get; set; }
        [SugarColumn(ColumnDescription = "备注")]
        public string Description { get; set; }
    }
}
