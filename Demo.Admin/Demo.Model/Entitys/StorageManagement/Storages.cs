﻿using Demo.Model.Common;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Entitys.StorageManagement
{
    public class Storages:IEntity
    {
        [SugarColumn(ColumnDescription = "入库单号")]
        public string No { get; set; }
        [SugarColumn(ColumnDescription = "产品名称")]
        public string GoodsName { get; set; }
        [SugarColumn(ColumnDescription = "产品规格")]
        public string Goods_Gg { get; set;}
        [SugarColumn(ColumnDescription = "产品单位")]
        public  string Good_Unit { get; set; }
        [SugarColumn(ColumnDescription = "产品数量")]
        public string Good_Sl { get; set; }
        [SugarColumn(ColumnDescription = "产品价格")]
        public decimal Good_Total { get; set; }
    }
}
