﻿using Demo.Model.Common;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Entitys.StorageManagement
{
    public class Storage:IBase
    {
        [SugarColumn(ColumnDescription = "入库单号")]
        public string No { get; set; }
        [SugarColumn(ColumnDescription = "单位id")]
        public long UnitId { get; set; }
        [SugarColumn(ColumnDescription = "付款方式id")]
        public long FkfsId { get; set; }
        [SugarColumn(ColumnDescription = "仓库id")]
        public long WareHouseId { get; set; }
        [SugarColumn(ColumnDescription = "开单人id")]
        public string UserId { get; set; }
        [SugarColumn(ColumnDescription = "合计金额")]
        public decimal Amount { get; set; }
        [SugarColumn(ColumnDescription = "开单日期")]
        public DateTime Kdrq { get; set; }
        [SugarColumn(ColumnDescription = "审核人id")]
        public string ShrId { get; set; }
        [SugarColumn(ColumnDescription = "审核日期")]
        public DateTime Shrq { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "备注")]
        public string Description { get; set; }


    }
}
