﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Entitys.BasicData
{
    public class Log : IBase
    {
        /// <summary>
        /// 操作账号
        /// </summary>
        public string Czzh { get; set; }
        /// <summary>
        /// 操作日期
        /// </summary>
        public DateTime DateTime { get; set; }
        /// <summary>
        /// 操作类型
        /// </summary>
        public string OpenType { get; set; }
        /// <summary>
        /// 操作路径
        /// </summary>
        public string CzLj { get; set; }
        /// <summary>
        /// ip
        /// </summary>
        public string Ip { get; set; }
    }
}
