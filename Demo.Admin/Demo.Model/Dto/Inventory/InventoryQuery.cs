﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.Inventory
{
    public class InventoryQuery
    {
        public long Id { get; set; }
        public long WareHouseId { get; set; }
        public string WareHouseName { get; set; }
        public long GoodsId { get; set; }
        public string GoodsName { get; set; }
        public string KuWei { get; set; }
        public long Kcsl { get; set; }
    }
}
