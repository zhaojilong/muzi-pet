﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Demo.Model.Entitys;
using SqlSugar;

namespace Demo.Model.Dto.ClassFy
{
    [SugarTable("ClassFy")]
    public class ClassFyQuery : IBase
    {
        /// <summary>
        /// 分类名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Num { get; set; }
        /// <summary>
        /// 父id
        /// </summary>
        public int Pid { get; set; }
        [SqlSugar.SugarColumn(IsIgnore =true)]
        public List<ClassFyQuery> Child { get; set; }
    }
    [SugarTable("ClassFy")]
    public class ClassFyQuery_Fj
    {
        /// <summary>
        /// 主键
        /// </summary>
        [SugarColumn(IsPrimaryKey = true, IsIdentity = true)]
        public long Id { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        [SugarColumn(ColumnName ="Name")]
        public string Title { get; set; }
        /// <summary>
        /// 序号
        /// </summary>
        public int Num { get; set; }
        /// <summary>
        /// 父id
        /// </summary>
        [SugarColumn(ColumnName = "Pid")]
        public int ParentId { get; set; }
        [SqlSugar.SugarColumn(IsIgnore = true)]
        public List<ClassFyQuery_Fj> Children { get; set; }
    }
}
