﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.ClassFy
{
    public class ClassFyEntity:IBase
    {
        public int Num { get; set; }
        public string Name { get; set; }
    }
}
