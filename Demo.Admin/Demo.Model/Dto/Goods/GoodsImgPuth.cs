﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.Goods
{
    public class GoodsImgPuth
    {
        /// <summary>
        /// 产品id
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ImgPath { get; set; }
        /// <summary>
        /// 删除地址
        /// </summary>
        public string DelImgPath { get; set; }
        /// <summary>
        /// 图片顺序
        /// </summary>
        public int Xh
        {
            get; set;
        }
    }
}
