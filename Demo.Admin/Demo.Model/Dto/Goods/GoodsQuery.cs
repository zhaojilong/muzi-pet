﻿using Demo.Model.Common;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.Goods
{
    [SugarTable("Goods")]
    public class GoodsQuery : IEntity
    {
        /// <summary>
        /// 分类id
        /// </summary>
        public int ClassFy_id { get; set; }
        /// <summary>
        /// 分类名称
        /// </summary>
        public string ClassFy_Name { get; set; }
        /// <summary>
        /// 商品名称
        /// </summary>
        public string Goods_Name { get; set; }
        /// <summary>
        /// 商品名称首字母
        /// </summary>
        public string Goods_JpName { get; set; }
        /// <summary>
        /// 商品编号
        /// </summary>
        public string Goods_Bh { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        public string Goods_Gg { get; set; }
        /// <summary>
        /// 商品单位
        /// </summary>
        public string goods_Unit { get; set; }
        /// <summary>
        /// 销售价格1
        /// </summary>
        public decimal Goods_Price1 { get; set; }
        /// <summary>
        /// 销售价格2
        /// </summary>
        public decimal Goods_Price2 { get; set; }
        /// <summary>
        /// 销售价格3
        /// </summary>
        public decimal Goods_Price3 { get; set; }
        /// <summary>
        /// 销售限价
        /// </summary>
        public decimal Saleslimit { get; set; }
        /// <summary>
        /// 图片地址
        /// </summary>
        [SugarColumn(IsNullable = true)]
        public string ImgPath { get; set; }
    }
}
