﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.Goods
{
    public class GoodsWhere:IPage
    {
        public string Goods_Tj { get; set; }
        public int ClassFy_id { get; set; }
        
    }
}
