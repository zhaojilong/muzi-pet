﻿using Demo.Model.Common;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.BusinessUnit
{
    public class UnitQuer:IPage
    {
        public string Name { get; set; }
        public long Category { get; set; }
        public long Status { get; set; }
    }
}
