﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.BusinessUnit
{
    public class UpBusinessUnit
    {
        public long Id { get; set; }
        public string UnitName { get; set; }
        [SugarColumn(ColumnDescription = "简拼")]
        public string UnitNameJp { get; set; }
        [SugarColumn(ColumnDescription = "类别 0代表供应商/销售商 1代表供应商 2代表销售商", DefaultValue = "0")]
        public long Category { get; set; }
        [SugarColumn(ColumnDescription = "公司电话")]
        public string CompanyPhone { get; set; }
        [SugarColumn(ColumnDescription = "地址")]
        public string Address { get; set; }
        [SugarColumn(ColumnDescription = "传真")]
        public string Fax { get; set; }

        [SugarColumn(ColumnDescription = "公户")]
        public string CardNo { get; set; }
        [SugarColumn(ColumnDescription = "状态 0代表使用 1代表禁用", DefaultValue = "0")]
        public long Status { get; set; }
        [SugarColumn(ColumnDescription = "备注")]
        public string UnitDescription { get; set; }
    }
}
