﻿using Demo.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Dto.WareHouse
{
    public class WareHouseEdit:IBase
    {
        public string Name { get; set; }
        public string IDCode { get; set; }
    }
}
