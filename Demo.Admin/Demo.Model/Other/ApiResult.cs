﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Other
{
    /// <summary>
    /// 统一接口返回模型
    /// </summary>
    public class ApiResult
    {
        public bool IsSuccess { get; set; }
        public object Result { get; set; }
        //public object Data { get; set; }
        public string Msg { get; set; }
        public int Code { get; set; }
        //public int Count { get; set; }
    }
    public class ApiResultLayui
    {
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
        public string Msg { get; set; }
        public int Code { get; set; }
        public int Count { get; set; }
    }
}
