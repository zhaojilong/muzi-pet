﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Other
{
    public class PageInfo
    {
        public int Total { get; set; }
        public object Data { get; set; }
    }
}
