﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Common
{
    public class IPage
    {
        public int page { get; set; }
        public int limit { get; set; }
    }
}
