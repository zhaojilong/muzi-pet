﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Model.Common
{
    public class IEntity: IBase
    {
        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(IsNullable = true,ColumnDescription ="描述")]
        public string Description { get; set; }
        /// <summary>
        /// 创建人Id
        /// </summary>
        [SugarColumn(IsNullable = false, ColumnDescription ="创建人id")]
        public long CreateUserId { get; set; }
        /// <summary>
        /// 创建日期
        /// </summary>
        [SugarColumn(IsNullable = false,DefaultValue = "getdate()", ColumnDescription = "创建日期") ]
        public DateTime CreateDate { get; set; }//=DateTime.Now;
        
        /// <summary>
        /// 修改人Id
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "修改人id")]
        public long ModifyUserId { get; set; }
        /// <summary>
        /// 修改日期
        /// </summary>
        [SugarColumn(IsNullable = true, ColumnDescription = "修改日期")]
        public DateTime? ModifyDate { get; set; }
        /// <summary>
        /// 是否删除
        /// </summary>
        [SugarColumn(IsNullable = false,DefaultValue ="0", ColumnDescription ="是否删除 0代表正常 1代表删除")]
        public int IsDeleted { get; set; }
    }
}
