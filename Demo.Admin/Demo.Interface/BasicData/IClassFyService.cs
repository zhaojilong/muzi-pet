﻿using Demo.Model.Dto.ClassFy;
using Demo.Model.Other;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface
{
    public interface IClassFyService
    {
        Task<Tuple<bool, string, dynamic>> AddClassFy(ClassFyAdd classFyAdd);
        Task<Tuple<bool, string, dynamic>> GetAClassFy();
        Task<Tuple<bool, string, dynamic>> GetAClassFy_Fj();
        Task<Tuple<bool, string, dynamic>> EditClassFy(ClassFyEntity classFyEntity);
        Task<Tuple<bool, string, dynamic>> DeleteClassFy(int id);
    }
}
