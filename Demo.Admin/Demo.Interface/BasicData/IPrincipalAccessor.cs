﻿using Demo.Model.Other;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface
{
    public interface IPrincipalAccessor
    {
        string? GetToken();
    }
    public class PrincipalAccessor:IPrincipalAccessor {
        private readonly IHttpContextAccessor _contextAccessor;

        public PrincipalAccessor(IHttpContextAccessor httpContext)
        {
            _contextAccessor = httpContext;
        }
        /// <summary>
        /// 获取请求头部的Token
        /// </summary>
        /// <returns></returns>
        public string? GetToken()
        {
            var token = _contextAccessor?.HttpContext?.Request.Headers["Authorization"].ToString();
            return token;
        }
    }


}
