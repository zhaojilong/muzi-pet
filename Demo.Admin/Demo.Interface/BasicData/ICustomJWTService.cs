﻿using Demo.Model.Dto.User;
using Demo.Model.Entitys;
using Model.Dto.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface ICustomJWTService
    {
        //获取Token
        string GetToken(UserRes user);
        UserList GetUsers(UserLogin userLogin);
    }
}
