﻿using Demo.Model.Dto.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface IInventoryService
    {
        Task<Tuple<bool, string>> ReplenishInventory();
        Task<Tuple<bool, string, dynamic>> ListAll(int id);
        Task<Tuple<bool, string>> UpdateKw(InventoryQuery inventory);
        Task<Tuple<bool, string>> DelWarehouse(int id);
    }
}
