﻿using Demo.Model.Entitys.BasicData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface ILogService
    {
        bool AddLog(Log log);
    }
}
