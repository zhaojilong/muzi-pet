﻿using Demo.Model.Entitys;
using Model.Dto.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface
{
    public interface IUserService
    {
        UserRes GetUser(string username,string pwd);
        List<Users> GetUserAll();
    }
}
