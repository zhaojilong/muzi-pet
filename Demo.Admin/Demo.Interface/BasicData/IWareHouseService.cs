﻿using Demo.Model.Dto.WareHouse;
using Demo.Model.Entitys.BasicData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface IWareHouseService
    {
        /// <summary>
        /// 888888
        /// </summary>
        /// <param name="wareHouse"></param>
        /// <returns></returns>
        int wareHouseAdd(WareHouseAdd wareHouse);
        bool wareHouseEdit(WareHouseEdit wareHouse);
        bool wareHouseDel(int id);
        WareHouse GetwareHouse(int id);
        List<WareHouse> GetwareHouseAll(WareHouseWhere where);

    }
}
