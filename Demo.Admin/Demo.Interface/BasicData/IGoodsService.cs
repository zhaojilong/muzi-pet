﻿using Demo.Model.Dto.Goods;
using Demo.Model.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface
{
    public interface IGoodsService
    {
        Task<Tuple<bool,int,dynamic>>GetListGoods(GoodsWhere goodsWhere);
        Task<Tuple<bool, string, dynamic>> GetGoods(int id);
        Task<Tuple<bool,string,dynamic>>GoodsAdd(GoodsAdd goods);
        Task<Tuple<bool, string, dynamic>> GoodsEdit(GoodsEdit edit);
        Task<Tuple<bool, string, dynamic>> GoodsDel(int id);
        Task<bool>Imgputh(GoodsImgPuth goodsImgPuth);
    }
}
