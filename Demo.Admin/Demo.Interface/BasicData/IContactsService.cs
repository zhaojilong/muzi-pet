﻿using Demo.Model.Entitys.BasicData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface IContactsService
    {
        Task<Tuple<bool, string, dynamic>> GetList(int id);
        Task<Tuple<bool, string, dynamic>> ContactsAdd(Contacts contacts);
        Task<Tuple<bool, string, dynamic>> ContactsEdit(Contacts contacts);
        Task<Tuple<bool, string, dynamic>> DelContacts(int id);
    }
}
