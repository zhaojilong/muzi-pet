﻿using Demo.Model.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface.BasicData
{
    public interface IFkfsService
    {
        Task<Tuple<bool, string, dynamic>> GetLIst(string fkfs);
        Task<Tuple<bool, string, dynamic>> FkfsAdd(Fkfs fkfs);
        Task<Tuple<bool, string, dynamic>> FkfsEdit(Fkfs fkfs);
        Task<Tuple<bool, string, dynamic>> DelFkfs(int id);

    }
}
