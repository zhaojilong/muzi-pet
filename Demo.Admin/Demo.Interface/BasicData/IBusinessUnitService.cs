﻿using Demo.Model.Dto.BusinessUnit;
using Demo.Model.Entitys.BasicData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Interface
{
    public interface IBusinessUnitService
    {
        Task<Tuple<bool, int, dynamic?>> GetBusinessUnits(UnitQuer unitQuer);
        Task<Tuple<bool, string, dynamic>> GetBusinessUnit(int id);
        Task<Tuple<bool, string, dynamic>> AddUnit(BusinessUnit businessUnit);
        Task<Tuple<bool, string, dynamic>> PutUnit(UpBusinessUnit businessUnit);
        Task<Tuple<bool, string, dynamic>> DelUnit(int id);
        Task<Tuple<bool, string>> PutStart(int id);
    }
}
