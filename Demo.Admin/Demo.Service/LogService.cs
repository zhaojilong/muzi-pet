﻿using AutoMapper;
using Demo.Interface.BasicData;
using Demo.Model.Entitys.BasicData;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service
{
    public class LogService : ILogService
    {
        private readonly ISqlSugarClient _db;

        public LogService(ISqlSugarClient db)
        {
            _db = db;
        }
        public bool AddLog(Log log)
        {
            var isok = _db.Insertable(log).ExecuteCommandIdentityIntoEntity();
            return isok;
        }
    }
}
