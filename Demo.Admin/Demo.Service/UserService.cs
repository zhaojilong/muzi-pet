﻿using AutoMapper;
using Demo.Interface;
using Model.Dto.User;
using SqlSugar;
using Demo.Model.Entitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Demo.Service
{
    public class UserService:IUserService
    {
        private readonly IMapper _mapper;
        private readonly ISqlSugarClient _db;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserService(IMapper mapper, ISqlSugarClient db, IHttpContextAccessor httpContextAccessor)
        {            
            _mapper = mapper;
            _db = db;
            _httpContextAccessor = httpContextAccessor;
        }


        public UserRes GetUser(string username, string pwd)
        {
            var user = _db.Queryable<Users>().Where(u => u.Name == username && u.Password == pwd).First();
            if (user != null)
            {
                return _mapper.Map<UserRes>(user);
            }

            return new UserRes();
        }

        public List<Users> GetUserAll()
        {
            var users = _db.Queryable<Users>().ToList();
            return users;
        }
    }
}
