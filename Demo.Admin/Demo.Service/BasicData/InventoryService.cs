﻿using Demo.Interface.BasicData;
using Demo.Model.Dto.Inventory;
using Demo.Model.Entitys;
using Demo.Model.Entitys.BasicData;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service.BasicData
{
    public class InventoryService : IInventoryService
    {
        private readonly ISqlSugarClient _db;
        public InventoryService(ISqlSugarClient db)
        {
            _db = db;
        }


        public async Task<Tuple<bool, string, dynamic>> ListAll(int id)
        {
            var info = _db.Queryable<Inventory>()
                .LeftJoin<Goods>((i, g) => i.GoodsId == g.Id)
                .LeftJoin<WareHouse>((i, g, w) => i.WareHouseId == w.Id)
                .Select((i, g, w) => new
                {
                    w.Id,
                    GoodsId = g.Id,
                    GoodsName = g.Goods_Name,
                    WareHouseId = w.Id,
                    WareHouseName = w.Name,
                    i.KuWei,
                    i.Kcsl,
                }).Where(i => i.GoodsId == id).ToList();
            if (info == null) return new(false, "没有改商品", info);
            return new Tuple<bool, string, dynamic>(true, "查询成功", info);
        }

        public async Task<Tuple<bool, string>> ReplenishInventory()
        {
            var Inventory1 = new List<Inventory>();
            var goods = _db.Queryable<Goods>().ToList();
            var warehouse = _db.Queryable<WareHouse>().ToList();
            var inven = _db.Queryable<Inventory>().ToList();
            foreach (var ware in warehouse)
            {
                foreach (Goods good in goods)
                {
                    if (inven.Where(t => t.GoodsId == good.Id && t.WareHouseId == ware.Id).Count() == 0)
                    {
                        var inventory1 = new Inventory();
                        inventory1.Id = good.Id;
                        inventory1.WareHouseId = ware.Id;
                        inventory1.GoodsId = good.Id;
                        inventory1.KuWei = "";
                        inventory1.Kcsl = 0;
                        Inventory1.Add(inventory1);
                    }
                }
            }
            if (Inventory1.Count() == 0)
            {
                return new(true, "无需补全");
            }
            else
            {
                var isok = _db.Insertable(Inventory1).ExecuteCommandIdentityIntoEntity();
                return new(isok, "补全成功");
            }

        }

        public async Task<Tuple<bool, string>> UpdateKw(InventoryQuery inventory)
        {
            var info = _db.Queryable<Inventory>().First(t => t.GoodsId == inventory.GoodsId && t.WareHouseId == inventory.WareHouseId);
            if (info == null) return new(false, "没有该实体");
            info.KuWei = inventory.KuWei;
            var isok = _db.Updateable(info).ExecuteCommandHasChange();
            return new(isok, "修改成功");
        }

        public async Task<Tuple<bool, string>> DelWarehouse(int id)
        {
            var info = _db.Queryable<Inventory>().First(t => t.Id == id);
            if (info == null) return new(false, "没有该实体");
            var isok = _db.Deleteable(info).ExecuteCommandHasChange();
            return new(isok, "删除成功");
        }
    }
}
