﻿using AutoMapper;
using Demo.Interface.BasicData;
using Demo.Model.Dto.WareHouse;
using Demo.Model.Entitys.BasicData;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Model.Dto.User;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service.BasicData
{
    public class WareHouseService : IWareHouseService
    {
        private readonly IMapper _mapper;
        private readonly ISqlSugarClient _db;
        public WareHouseService(ISqlSugarClient sqlSugarClient, IMapper _mapper)
        {
            _db = sqlSugarClient;
            _mapper = _mapper;
        }

        public WareHouse GetwareHouse(int id)
        {
            var info = _db.Queryable<WareHouse>().Where(w => w.Id == id).First();
            return info;
        }

        public List<WareHouse> GetwareHouseAll(WareHouseWhere where)
        {
            var list = _db.Queryable<WareHouse>().Where(t => t.Name.Contains(where.Name)).ToList();
            return list;
        }

        public int wareHouseAdd(WareHouseAdd wareHouse)
        {

            var id = _db.Insertable<WareHouse>(wareHouse).ExecuteReturnIdentity();
            //if (id!=null)
            //{
            //    return _mapper.Map<WareHouse>(id);
            //}
            return id;
        }

        public bool wareHouseDel(int id)
        {
            var isok = _db.Deleteable<WareHouse>(id).ExecuteCommand() > 0;
            return isok;
        }

        public bool wareHouseEdit(WareHouseEdit wareHouse)
        {
            var isok = _db.Updateable<WareHouse>(wareHouse).Where(c => c.Id == wareHouse.Id).ExecuteCommand() > 0;
            return isok;
        }
    }
}
