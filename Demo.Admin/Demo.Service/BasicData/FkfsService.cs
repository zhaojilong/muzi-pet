﻿using Demo.Interface;
using Demo.Interface.BasicData;
using Demo.Model.Entitys;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service.BasicData
{
    /// <summary>
    /// 付款方式
    /// </summary>
    public class FkfsService : IFkfsService
    {
        private readonly ISqlSugarClient _db;
        public FkfsService(ISqlSugarClient sqlSugar)
        {
            _db = sqlSugar;
        }
        public async Task<Tuple<bool, string, dynamic>> DelFkfs(int id)
        {
            var isok = _db.Deleteable<Fkfs>(t => t.Id == id).ExecuteCommandHasChange();
            if (isok)
                return new(isok, "删除成功", true);
            else
                return new(isok, "删除失败", false);
        }

        public async Task<Tuple<bool, string, dynamic>> FkfsAdd(Fkfs fkfs)
        {
            var isok = _db.Insertable(fkfs).ExecuteCommandIdentityIntoEntity();
            if (isok)
                return new(isok, "新增成功", true);
            else
                return new(isok, "新增失败", false);
        }

        public async Task<Tuple<bool, string, dynamic>> FkfsEdit(Fkfs fkfs)
        {
            var isok = _db.Updateable(fkfs).ExecuteCommandHasChange();
            if (isok)
                return new(isok, "修改成功", true);
            else
                return new(isok, "修改失败", false);
        }



        public async Task<Tuple<bool, string, dynamic>> GetLIst(string fkfs)
        {
            var list = _db.Queryable<Fkfs>().Where(t => t.Name.Contains(fkfs)).ToList();
            return new(true, "查询成功", list);
        }

        //public async Tuple<bool, string, dynamic> GetLIst(Fkfs fkfs)
        //{
        //    var list = _db.Queryable<Fkfs>().ToList();
        //    return new(true, "查询成功", list);
        //}
    }
}
