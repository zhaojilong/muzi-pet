﻿using AutoMapper;
using Demo.Interface;
using Demo.Model.Dto.ClassFy;
using Demo.Model.Entitys.BasicData;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service.BasicData
{
    public class ClassFyService : IClassFyService
    {
        private readonly ISqlSugarClient _db;
        private readonly IMapper _mapper;
        public ClassFyService(ISqlSugarClient sqlSugarClient)
        {
            _db = sqlSugarClient;
        }

        public async Task<Tuple<bool, string, dynamic>> AddClassFy(ClassFyAdd classFyAdd)
        {
            var info = _db.Queryable<ClassFy>().Where(t => t.Name == classFyAdd.Name).Count();
            if (info > 0) return new Tuple<bool, string, dynamic>(false, "名称不能重复", null);
            var isok = await _db.Insertable<ClassFy>(classFyAdd).ExecuteCommandIdentityIntoEntityAsync();
            if (isok)
            {
                return new Tuple<bool, string, dynamic>(isok, "添加成功", true);
            }
            else
            {
                return new Tuple<bool, string, dynamic>(isok, "添加失败", false);
            }

        }

        public async Task<Tuple<bool, string, dynamic>> DeleteClassFy(int id)
        {
            if (id == 1) return new(false, "主分类不能被删除", false);
            var info = _db.Queryable<ClassFy>().Where(t => t.Id == id).Count();
            if (info == 0) return new(false, "没有实体", false);
            var infoxj = _db.Queryable<ClassFy>().Where(t => t.Pid == id).Count();
            if (infoxj > 0) return new(false, "该分类下有子集不能被删除", false);
            try
            {
                var isok = await _db.Deleteable<ClassFy>(id).ExecuteCommandHasChangeAsync();
                if (isok)
                {
                    return new Tuple<bool, string, dynamic>(isok, "删除成功", true);
                }
                else
                {
                    return new Tuple<bool, string, dynamic>(isok, "删除失败", false);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, dynamic>(false, "删除失败" + ex.GetBaseException, false);
            }
        }

        public async Task<Tuple<bool, string, dynamic>> EditClassFy(ClassFyEntity classFyEntity)
        {
            try
            {
                var info = _db.Queryable<ClassFy>().Where(t => t.Name == classFyEntity.Name && t.Id != classFyEntity.Id).Count();
                if (info > 0) return new Tuple<bool, string, dynamic>(false, "名称不能重复", null);
                var isok = await _db.Updateable<ClassFy>(classFyEntity).ExecuteCommandHasChangeAsync();
                if (isok)
                {
                    return new Tuple<bool, string, dynamic>(isok, "修改成功", true);
                }
                else
                {
                    return new Tuple<bool, string, dynamic>(isok, "修改失败", false);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, dynamic>(false, "修改失败" + ex.GetBaseException(), false);
            }
        }

        public async Task<Tuple<bool, string, dynamic>> GetAClassFy()
        {
            //var data = _db.Queryable<ClassFyQuery>().OrderBy(t => t.Num).ToTree(t => t.Child, t => t.Pid, -1);
            var data = _db.Queryable<ClassFyQuery>().OrderBy(t => t.Num).ToList();
            return new(true, "", data);
        }

        public async Task<Tuple<bool, string, dynamic>> GetAClassFy_Fj()
        {
            var data = await _db.Queryable<ClassFyQuery_Fj>().OrderBy(t => t.Num).ToTreeAsync(t => t.Children, t => t.ParentId, -1);


            //var data = _db.Queryable<ClassFyQuery>().OrderBy(t => t.Num).ToList();
            return new(true, "", data);
        }
    }
}
