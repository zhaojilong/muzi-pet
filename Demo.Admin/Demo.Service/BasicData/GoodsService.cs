﻿using Demo.Interface;
using Demo.Model.Dto.Goods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlSugar;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using Demo.Model.Dto.ClassFy;
using Demo.Model.Other;
using Demo.Model.Entitys.BasicData;

namespace Demo.Service.BasicData
{
    public class GoodsService : IGoodsService
    {
        private readonly ISqlSugarClient _db;
        private readonly IMapper _mapper;
        public GoodsService(ISqlSugarClient db, IClassFyService classFy)
        {
            _db = db;
        }

        public async Task<Tuple<bool, string, dynamic>> GetGoods(int id)
        {

            var data = _db.Queryable<Goods>().First(it => it.Id == id);
            if (data != null)
            {
                return new(true, "查询成功", data);
            }
            else
            {
                return new(false, "查询成功", null);
            }

        }

        public async Task<Tuple<bool, int, dynamic>> GetListGoods(GoodsWhere goodsWhere)
        {
            List<GoodsQuery> query = new List<GoodsQuery>();
            //查询下集分类
            var ClassFyx = _db.Queryable<ClassFy>().ToChildList(t => t.Pid, goodsWhere.ClassFy_id).ToList();
            foreach (var item in ClassFyx)
            {
                var list = _db.Queryable<Goods>()
                 .LeftJoin<ClassFy>((s, f) => s.ClassFy_id == f.Id)
                 .Where(s => s.Goods_Bh.Contains(goodsWhere.Goods_Tj) || s.Goods_Name.Contains(goodsWhere.Goods_Tj) || s.Goods_JpName.Contains(goodsWhere.Goods_Tj)).Where(s => s.IsDeleted == 0 && s.ClassFy_id == item.Id)
                 .Select((s, f) => new GoodsQuery
                 {
                     Id = s.Id,
                     ClassFy_id = s.ClassFy_id,
                     ClassFy_Name = f.Name,
                     Goods_Name = s.Goods_Name,
                     Goods_JpName = s.Goods_JpName,
                     Goods_Bh = s.Goods_Bh,
                     Goods_Gg = s.Goods_Gg,
                     goods_Unit = s.goods_Unit,
                     Goods_Price1 = s.Goods_Price1,
                     Goods_Price2 = s.Goods_Price2,
                     Goods_Price3 = s.Goods_Price3,
                     Saleslimit = s.Saleslimit,
                     ImgPath = s.ImgPath,
                     Description = s.Description,
                     CreateUserId = s.CreateUserId,
                     CreateDate = s.CreateDate,
                     ModifyDate = s.ModifyDate,
                     ModifyUserId = s.ModifyUserId,
                     IsDeleted = s.IsDeleted
                 }).ToPageList(goodsWhere.page, goodsWhere.limit);
                query.AddRange(list);
            }



            //引入配置文件 获取全局控制链接地址
            var _config = new ConfigurationBuilder()
                             .SetBasePath(AppContext.BaseDirectory)
                             .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                             .Build();
            var http = _config.GetConnectionString("Http");
            //循环匹配地址
            query.ForEach(t => { t.ImgPath = http + t.ImgPath; });
            //list = ClassFyEach(list, goodsWhere.ClassFy_id);
            return new(true, query.Count, query.OrderBy(t => t.Goods_Name));
        }

        public async Task<Tuple<bool, string, dynamic>> GoodsAdd(GoodsAdd goods)
        {
            var info = _db.Queryable<Goods>().Where(t => t.Goods_Name == goods.Goods_Name).Count();
            if (info > 0) return new Tuple<bool, string, dynamic>(false, "名称不能重复", false);
            try
            {
                var isok = await _db.Insertable<Goods>(goods).ExecuteCommandIdentityIntoEntityAsync();

                if (isok)
                {
                    return new Tuple<bool, string, dynamic>(isok, "添加成功", true);
                }
                else
                {
                    return new Tuple<bool, string, dynamic>(isok, "添加失败", false);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, dynamic>(false, "添加失败" + ex.GetBaseException(), false);
            }
        }

        public async Task<Tuple<bool, string, dynamic>> GoodsDel(int id)
        {
            var info = _db.Queryable<Goods>().Any(t => t.Id == id);
            if (!info) return new(false, "没有该实体", null);
            try
            {
                var data = _db.Queryable<Goods>().First(t => t.Id == id);
                data.IsDeleted = 1;
                var data2 = _db.Updateable(data).ExecuteCommandHasChange();
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, dynamic>(false, "删除失败" + ex.GetBaseException(), false);
            }
            return new Tuple<bool, string, dynamic>(true, "删除成功", null);
        }

        public async Task<Tuple<bool, string, dynamic>> GoodsEdit(GoodsEdit edit)
        {
            var info = _db.Queryable<Goods>().Any(t => t.Id == edit.Id);
            if (!info) return new(false, "没有该实体", null);
            var data = _db.Queryable<Goods>().Any(t => t.Goods_Name == edit.Goods_Name && t.Id != edit.Id);
            if (data) return new(false, "名称不能重复", null);
            try
            {
                var isok = _db.Updateable<Goods>(edit).ExecuteCommandHasChange();
                if (isok)
                {
                    return new Tuple<bool, string, dynamic>(isok, "修改成功", true);
                }
                else
                {
                    return new Tuple<bool, string, dynamic>(isok, "修改失败", false);
                }
            }
            catch (Exception ex)
            {
                return new Tuple<bool, string, dynamic>(false, "修改失败" + ex.GetBaseException(), false);
            }
        }

        public async Task<bool> Imgputh(GoodsImgPuth goodsImgPuth)
        {
            var isok = false;
            var info = _db.Queryable<Goods>().First(t => t.Id == goodsImgPuth.Id);
            goodsImgPuth.ImgPath = goodsImgPuth.ImgPath.Replace("wwwroot", "");
            switch (goodsImgPuth.Xh)
            {
                case 1:
                    if (info.ImgPath.Length > 0)
                    {
                        UploadDemo.FileHelper.FileDel(goodsImgPuth.DelImgPath + info.ImgPath);
                        info.ImgPath = goodsImgPuth.ImgPath;
                    }
                    else
                    {
                        info.ImgPath = goodsImgPuth.ImgPath;
                    }
                    break;
                case 2:
                    if (info.ImgPath2 != null)
                    {
                        UploadDemo.FileHelper.FileDel(goodsImgPuth.DelImgPath + info.ImgPath2);
                        info.ImgPath2 = goodsImgPuth.ImgPath;
                    }
                    else
                    {
                        info.ImgPath2 = goodsImgPuth.ImgPath;
                    }
                    break;
                case 3:
                    if (info.ImgPath3 != null)
                    {
                        UploadDemo.FileHelper.FileDel(goodsImgPuth.DelImgPath + info.ImgPath3);
                        info.ImgPath3 = goodsImgPuth.ImgPath;
                    }
                    else
                    {
                        info.ImgPath3 = goodsImgPuth.ImgPath;
                    }
                    break;
                case 4:
                    if (info.ImgPath4 != null)
                    {
                        UploadDemo.FileHelper.FileDel(goodsImgPuth.DelImgPath + info.ImgPath4);
                        info.ImgPath4 = goodsImgPuth.ImgPath;
                    }
                    else
                    {
                        info.ImgPath4 = goodsImgPuth.ImgPath;
                    }
                    break;
                default:
                    isok = false;
                    break;
            }
            isok = _db.Updateable(info).ExecuteCommandHasChange();
            return isok;
        }
    }
}
