﻿using Demo.Interface;
using Demo.Interface.BasicData;
using Demo.Model.Entitys.BasicData;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo.Service.BasicData
{
    public class ContactsService : IContactsService
    {
        private readonly ISqlSugarClient _db;
        public ContactsService(ISqlSugarClient db)
        {
            _db = db;
        }
        public async Task<Tuple<bool, string, dynamic>> ContactsAdd(Contacts contacts)
        {
            var isok = _db.Insertable(contacts).ExecuteCommandIdentityIntoEntity();
            if (isok)
                return new(isok, "新增成功", true);
            else
                return new(isok, "新增失败", false);

        }

        public async Task<Tuple<bool, string, dynamic>> ContactsEdit(Contacts contacts)
        {
            var isok = _db.Updateable(contacts).ExecuteCommandHasChange();
            if (isok)
                return new(isok, "修改成功", true);
            else
                return new(isok, "修改失败", false);
        }

        public async Task<Tuple<bool, string, dynamic>> DelContacts(int id)
        {
            var isok = _db.Deleteable<Contacts>(t => t.Id == id).ExecuteCommandHasChange();
            if (isok)
                return new(isok, "删除成功", true);
            else
                return new(isok, "删除失败", false);
        }

        public async Task<Tuple<bool, string, dynamic>> GetList(int id)
        {
            var list = _db.Queryable<Contacts>().Where(t => t.UnitId == id).ToList();
            return new(true, "查询成功", list);
        }
    }
}
