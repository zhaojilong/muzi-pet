﻿

    /// <summary>
    /// Api枚举类
    /// </summary>
    public enum APIVersion
    {
        /// <summary>
        /// v1版本
        /// </summary>
        v1,
        /// <summary>
        /// v2版本
        /// </summary>
        v2
    }

