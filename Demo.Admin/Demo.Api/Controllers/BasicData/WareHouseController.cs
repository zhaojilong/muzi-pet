﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface.BasicData;
using Demo.Model.Dto.WareHouse;
using Demo.Model.Entitys;
using Demo.Model.Other;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 仓库
    /// </summary>
    ///[Authorize]
    public class WareHouseController : WebApiBaseController
    {
        private readonly IWareHouseService _warehouseService;
        private readonly ILogger<WareHouseController> _logger;
        public WareHouseController(IWareHouseService warehouseService, ILogger<WareHouseController> logger) : base()
        {
            _warehouseService = warehouseService;
            _logger = logger;
        }
        /// <summary>
        /// 查询所有仓库
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResultLayui> GetAll(WareHouseWhere where)
        {
            var list = _warehouseService.GetwareHouseAll(where);
            return ResultHelper.Successlayui(list, 0, "请求成功");
        }
        /// <summary>
        /// 根据id查询仓库
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ApiResult> Get99(int id)
        {
            var dt = _warehouseService.GetwareHouse(id);
            //_logger.LogInformation(this.GetType()+"查询仓库");
            //_logger.LogError("查不到");
            return ResultHelper.Success(dt, "请求成功");
        }

        /// <summary>
        /// 新增仓库
        /// </summary>
        /// <param name="wareHouse"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> Post(WareHouseAdd wareHouse)
        {
            var res = Task.Run(() =>
              {
                  if (string.IsNullOrEmpty(wareHouse.IDCode) || string.IsNullOrEmpty(wareHouse.Name))
                  {
                      return ResultHelper.Error("参数不能为空");
                  }
                  var id = _warehouseService.wareHouseAdd(wareHouse);
                  return ResultHelper.Success(id, "请求成功");
              });
            //var model = _warehouseService.wareHouseAdd(wareHouse);
            return await res;
        }

        /// <summary>
        /// 修改仓库
        /// </summary>
        /// <param name="wareHouse"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> Put(WareHouseEdit wareHouse)
        {
            var res = Task.Run(() =>
            {
                if (string.IsNullOrEmpty(wareHouse.IDCode) || string.IsNullOrEmpty(wareHouse.Name))
                {
                    return ResultHelper.Error("参数不能为空");
                }
                var info = _warehouseService.wareHouseEdit(wareHouse);
                return ResultHelper.Success(info, "请求成功");
            });

            return await res;
        }

        /// <summary>
        /// 删除仓库
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public async Task<ApiResult> Delete(int id)
        {
            var res = Task.Run(() =>
            {
                if (id == null)
                {
                    return ResultHelper.Error("id不能为空");
                }
                var isok = _warehouseService.wareHouseDel(id);
                return ResultHelper.Success(isok, "请求成功");

            });
            return await res;
        }
    }
}
