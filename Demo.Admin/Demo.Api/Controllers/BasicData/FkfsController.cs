﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface.BasicData;
using Demo.Model.Entitys;
using Demo.Model.Other;
using Demo.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 付款方式
    /// </summary>
    public class FkfsController : WebApiBaseController
    {
        private readonly IFkfsService _fkfs;
        public FkfsController(IFkfsService fkfss)
        {
            _fkfs = fkfss;
        }
        /// <summary>
        /// 查询付款方式
        /// </summary>
        /// <param name="fkfs"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResultLayui> GetList(Fkfs fkfs)
        {
            var data = await _fkfs.GetLIst(fkfs.Name);
            return ResultHelper.Successlayui(data.Item3, 0, data.Item2);


        }
        /// <summary>
        /// 新增
        /// </summary>
        /// <param name="fkfs"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> FkfsAdd(Fkfs fkfs)
        {
            var res = Task.Run(async () =>
             {

                 var data = await _fkfs.FkfsAdd(fkfs);
                 return ResultHelper.Success(data.Item3, data.Item2);
             });
            return await res;
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="fkfs"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> FkfsEntity(Fkfs fkfs)
        {
            return await Task.Run(async () =>
            {

                var data = await _fkfs.FkfsEdit(fkfs);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="fkfs"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> DelFkfs(int id)
        {
            var res = Task.Run(async () =>
            {

                var data = await _fkfs.DelFkfs(id);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
            return await res;
        }
    }
}
