﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface;
using Demo.Model.Dto.BusinessUnit;
using Demo.Model.Entitys.BasicData;
using Demo.Model.Other;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Org.BouncyCastle.Crypto.Modes.Gcm;

namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 业务单位
    /// </summary>
    public class BusinessUnitController : WebApiBaseController
    {
        private readonly IBusinessUnitService _unitService;
        public BusinessUnitController(IBusinessUnitService unitService)
        {
            _unitService = unitService;
        }
        /// <summary>
        /// 查询业务单位
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResultLayui> GetUnit(UnitQuer unitQuer)
        {
            var res = Task.Run(async () =>
            {
                var result = await _unitService.GetBusinessUnits(unitQuer);
                return ResultHelper.Successlayui(result.Item3, result.Item2, "成功");
            });
            return await res;
        }
        ///
        [HttpGet]
        public async Task<ApiResult> GetUnitid(int id)
        {
            return await Task.Run(async () =>
            {
                var result = await _unitService.GetBusinessUnit(id);
                return ResultHelper.Success(result.Item3, result.Item2);
            });
        }
        /// <summary>
        /// 修改业务单
        /// </summary>
        /// <param name="unitQuer"></param>
        /// <returns></returns>
        [HttpPut]
        public Task<ApiResult> PutUnit(UpBusinessUnit unitQuer)
        {
            var res = Task.Run(async () =>
            {
                if (unitQuer.Id == null)
                {
                    return ResultHelper.Error("id不能为空");
                }
                var result = await _unitService.PutUnit(unitQuer);
                return ResultHelper.Success(result.Item1, result.Item2);
            });
            return res;
        }
        /// <summary>
        /// 新增业务单位
        /// </summary>
        /// <param name="unit"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> AddUnit(BusinessUnit unit)
        {
            return Task.Run(async () =>
            {
                var result = await _unitService.AddUnit(unit);
                return ResultHelper.Success(result.Item1, result.Item2);
            });
        }
        /// <summary>
        /// 供应商删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public Task<ApiResult> DelUnit(int id)
        {
            return Task.Run(async () =>
            {
                var isok = await _unitService.DelUnit(id);
                return ResultHelper.Success(isok.Item1, isok.Item2);
            });
        }
        /// <summary>
        /// 启用禁用业务单位
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> PutStart(int id)
        {
            return await Task.Run(async () =>
            {
                var isok = await _unitService.PutStart(id);
                return ResultHelper.Success(isok.Item1, isok.Item2);
            });
        }
    }
}
