﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface.BasicData;
using Demo.Model.Entitys.BasicData;
using Demo.Model.Other;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;


namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 业务单位--->联系人
    /// </summary>
    public class ContactController : WebApiBaseController
    {
        private readonly IContactsService _contactsService;
        public ContactController(IContactsService contactsService)
        {
            _contactsService = contactsService;
        }
        /// <summary>
        ///联系人明细查询
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResultLayui> GetContact(int id)
        {
            var res = Task.Run(async () =>
            {
                var data = await _contactsService.GetList(id);
                return ResultHelper.Successlayui(data.Item3, 0, data.Item2);
            });
            return await res;
        }


        /// <summary>
        /// 联系人新增
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> ContactAdd(Contacts contacts)
        {
            var res = Task.Run(async () =>
            {
                var data = await _contactsService.ContactsAdd(contacts);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
            return await res;
        }

        /// <summary>
        /// 联系人修改
        /// </summary>
        /// <param name="contacts"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> ContactEdit(Contacts contacts)
        {
            var res = Task.Run(async () =>
            {
                var data = await _contactsService.ContactsEdit(contacts);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
            return await res;
        }

        /// <summary>
        /// 联系人删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ApiResult> DelContact(int id)
        {
            var res = Task.Run(async () =>
            {
                var data = await _contactsService.DelContacts(id);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
            return await res;
        }
    }
}
