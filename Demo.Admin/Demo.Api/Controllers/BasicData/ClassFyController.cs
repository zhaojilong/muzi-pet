﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface;
using Demo.Model.Dto.ClassFy;
using Demo.Model.Other;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 商品分类
    /// </summary>
    public class ClassFyController : WebApiBaseController
    {
        private readonly IClassFyService _classFyService;
        public ClassFyController(IClassFyService classFyService)
        {
            _classFyService = classFyService;
        }
        /// <summary>
        /// 新增分类
        /// </summary>
        /// <param name="classFy"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ApiResult> ClassFyAdd(ClassFyAdd classFy)
        {
            var res = Task.Run(async () =>
            {
                if (string.IsNullOrEmpty(classFy.Num.ToString()) || string.IsNullOrEmpty(classFy.Name))
                {
                    return ResultHelper.Error("参数不能为空");
                }
                var list = await _classFyService.AddClassFy(classFy);
                return ResultHelper.Success(list.Item3, list.Item2);
            });
            return await res;
        }
        /// <summary>
        /// 查询分类
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResultLayui> GetClassFy()
        {
            var res = Task.Run(async () =>
            {
                var data = await _classFyService.GetAClassFy();
                return ResultHelper.Successlayui(data.Item3, 999, data.Item2);
            });
            return await res;
        }
        /// <summary>
        /// 查询分类分级
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResultLayui> GetClassFy_Fj()
        {
            var res = Task.Run(async () =>
            {
                var data = await _classFyService.GetAClassFy_Fj();
                return ResultHelper.Successlayui(data.Item3, 999, data.Item2);
            });
            return await res;
        }
        /// <summary>
        /// 修改分类
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ApiResult> EditClassFy(ClassFyEntity classFy)
        {
            var res = Task.Run(async () =>
            {
                if (string.IsNullOrEmpty(classFy.Num.ToString()) && string.IsNullOrEmpty(classFy.Name))
                {
                    return ResultHelper.Error("参数不能为空");
                }
                var data = await _classFyService.EditClassFy(classFy);
                return ResultHelper.Success(data.Item3, data.Item2);
            });
            return await res;
        }
        /// <summary>
        /// 删除分类
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ApiResult> DelClassFy(int id)
        {
            var res = Task.Run(async () =>
            {
                var data = await _classFyService.DeleteClassFy(id);
                return ResultHelper.Success(data.Item3, data.Item2);

            });
            return await res;
        }

    }
}
