﻿using Demo.Api.Config;
using Demo.Api.Controllers.Commin;
using Demo.Interface.BasicData;
using Demo.Model.Dto.Inventory;
using Demo.Model.Other;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Demo.Api.Controllers.BasicData
{
    /// <summary>
    /// 库存管理
    /// </summary>
    public class InventoryController : WebApiBaseController
    {
        private readonly IInventoryService _inventory;
        public InventoryController(IInventoryService inventory)
        {
            _inventory = inventory;
        }
        /// <summary>
        /// 数据仓库补全
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult> ReplenishInventory()
        {
            var res = Task.Run(async () =>
            {
                var data = await _inventory.ReplenishInventory();
                return ResultHelper.Success(data.Item1, data.Item2);

            });
            return res;
        }
        /// <summary>
        /// 根据商品查询所属仓库
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResultLayui> ListAll(int id)
        {
            var data = await _inventory.ListAll(id);
            return ResultHelper.Successlayui(data.Item3, 0, data.Item2);

        }
        /// <summary>
        /// 根据商品修改库位
        /// </summary>
        /// <param name="inventory"></param>
        /// <returns></returns>
        [HttpPut]
        public Task<ApiResult> UpdateKw(InventoryQuery inventory)
        {
            var res = Task.Run(async () =>
            {
                var isok = await _inventory.UpdateKw(inventory);
                return ResultHelper.Success(isok.Item1, isok.Item2);
            });
            return res;
        }
        ///// <summary>
        ///// 删除仓库
        ///// </summary>
        ///// <param name="id"></param>
        ///// <returns></returns>
        //[HttpDelete]
        //public Task<ApiResult> DelWareHous(int id)
        //{
        //    var res = Task.Run(async () =>
        //    {
        //        var isok=await _inventory.DelWarehouse(id);
        //        return ResultHelper.Success(isok.Item1, isok.Item2);
        //    });
        //    return res;
        //}
    }
}
