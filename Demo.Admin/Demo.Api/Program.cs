using Demo.Api.Config;
using Demo.Model.Other;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);
//var basePath = AppContext.BaseDirectory;

//builder.Services.AddControllersWithViews();
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
#region swagger注释，版本信息控制
builder.Services.AddSwaggerGen(options =>
{
    //获取xml文件名称
    var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName), true);
    typeof(APIVersion).GetEnumNames().ToList().ForEach(version =>
    {
        //添加文档介绍
        options.SwaggerDoc(version, new OpenApiInfo
        {
            Title = "zjlNet6项目",
            Version = version.ToString(),
            Description = $"zjlNet6项目:{version}版本"
        });

    });

    //添加tokan设置
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "Value: Bearer {token}",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement()
    {
      {
        new OpenApiSecurityScheme
        {
          Reference = new OpenApiReference
          {
            Type = ReferenceType.SecurityScheme,
            Id = "Bearer"
          },Scheme = "oauth2",Name = "Bearer",In = ParameterLocation.Header,
        },new List<string>()
      }
    });
});
#endregion
builder.Register();
builder.Services.AddBStyle(c=>c.UseDefault());


//初始化Redis



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{  
    app.UseSwagger();
    //版本切换
    app.UseSwaggerUI(options =>
    {
        typeof(APIVersion).GetEnumNames().ToList().ForEach(version =>
        {
            options.SwaggerEndpoint($"/swagger/{version}/swagger.json", $"版本选择:{version}");
        });
    });
}
#region 启用静态资源访问
//创建目录
//var path = Path.Combine(AppContext.BaseDirectory, "Uploads/");
//UploadDemo.FileHelper.CreatePath(path);
////添加MIME支持
//var provider = new FileExtensionContentTypeProvider();
//provider.Mappings.Add(".fbx", "application/octet-stream");
//provider.Mappings.Add(".obj", "application/octet-stream");
//provider.Mappings.Add(".mtl", "application/octet-stream");
//app.UseStaticFiles(new StaticFileOptions
//{
//    FileProvider = new PhysicalFileProvider(path),
//    ContentTypeProvider = provider,
//    RequestPath = "/Uploads"
//});

//var evn = IWebHostEnvironment();
//var staticFile = new StaticFileOptions();
//staticFile.FileProvider = new PhysicalFileProvider();//指定静态文件目录
//                                                                        //开启静态文件服务  默认为wwwroot路径
app.UseStaticFiles();
#endregion
//鉴全授权
app.UseAuthentication();
app.UseAuthorization();
//使用策略
app.UseCors("CorsPolicy");

app.MapControllers();

app.Run();
