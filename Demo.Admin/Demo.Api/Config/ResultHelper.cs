﻿using Demo.Model.Other;

namespace Demo.Api.Config
{
    public class ResultHelper
    {
        public static ApiResult Success(object res,string msg)
        {
            if (msg == "") msg = "请求成功";
            return new ApiResult() { Code=0,IsSuccess = true, Result = res ,Msg= msg };
        }
        public static ApiResultLayui Successlayui(object res,int count, string msg)
        {
            if (msg == "") msg = "请求成功";
            return new ApiResultLayui() { Code = 0, Data = res, IsSuccess = true, Count = count, Msg = msg };
        }
        public static ApiResult Error(string message)
        {
            return new ApiResult() { IsSuccess = false, Msg = message };
        }
    }
}
