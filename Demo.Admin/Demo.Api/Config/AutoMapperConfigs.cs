﻿using AutoMapper;
using Demo.Model.Dto.BusinessUnit;
using Demo.Model.Dto.ClassFy;
using Demo.Model.Dto.Goods;
using Demo.Model.Dto.WareHouse;
using Demo.Model.Entitys;
using Demo.Model.Entitys.BasicData;
using Model.Dto.Menu;
using Model.Dto.Role;
using Model.Dto.User;

namespace Demo.Api.Config
{
    /// <summary>
    /// Dto的映射配置
    /// </summary>
    public class AutoMapperConfigs : Profile
    {
        public AutoMapperConfigs()
        {
            //角色
            CreateMap<Role, RoleRes>();
            CreateMap<RoleAdd, Role>();
            CreateMap<RoleEdit, Role>();
            //用户
            CreateMap<Users, UserRes>();
            CreateMap<UserAdd, Users>();
            CreateMap<UserEdit, Users>();
            //菜单
            CreateMap<Menu, MenuRes>();
            CreateMap<MenuAdd, Menu>();
            CreateMap<MenuEdit, Menu>();
            CreateMap<MenuEdit, Menu>();
            //仓库
            CreateMap<WareHouse, WareHouseAdd>();
            //分类
            CreateMap<ClassFy, ClassFyAdd>();
            CreateMap<ClassFy, ClassFyQuery>();
            CreateMap<ClassFy, ClassFyQuery_Fj>();
            //商品
            CreateMap<Goods, GoodsAdd>();
            CreateMap<Goods, GoodsEdit>();
            CreateMap<Goods, GoodsQuery>();
            //供应商
            CreateMap<UpBusinessUnit, BusinessUnit>();
        }
    }
}
