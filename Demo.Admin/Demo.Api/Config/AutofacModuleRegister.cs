﻿using Autofac;
using System.Reflection;

namespace Demo.Api.Config
{
    public class AutofacModuleRegister : Autofac.Module
    {
        //注册接口和实现之间的关系
        protected override void Load(ContainerBuilder builder)
        {
            Assembly interfaceAssembly = Assembly.Load("Demo.Interface");
            Assembly serviceAssembly = Assembly.Load("Demo.Service");
            //注册接口和实现层
            builder.RegisterAssemblyTypes(interfaceAssembly, serviceAssembly).AsImplementedInterfaces();
            //base.Load(builder); 

        }
    }
}
